// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, AgreementKind, AgreementSection};
use glib::{prelude::*, translate::*};

glib::wrapper! {
    #[doc(alias = "AsAgreement")]
    pub struct Agreement(Object<ffi::AsAgreement, ffi::AsAgreementClass>);

    match fn {
        type_ => || ffi::as_agreement_get_type(),
    }
}

impl Agreement {
    pub const NONE: Option<&'static Agreement> = None;

    #[doc(alias = "as_agreement_new")]
    pub fn new() -> Agreement {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::as_agreement_new()) }
    }
}

impl Default for Agreement {
    fn default() -> Self {
        Self::new()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Agreement>> Sealed for T {}
}

pub trait AgreementExt: IsA<Agreement> + sealed::Sealed + 'static {
    #[doc(alias = "as_agreement_add_section")]
    fn add_section(&self, agreement_section: &impl IsA<AgreementSection>) {
        unsafe {
            ffi::as_agreement_add_section(
                self.as_ref().to_glib_none().0,
                agreement_section.as_ref().to_glib_none().0,
            );
        }
    }

    #[doc(alias = "as_agreement_get_kind")]
    #[doc(alias = "get_kind")]
    fn kind(&self) -> AgreementKind {
        unsafe { from_glib(ffi::as_agreement_get_kind(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "as_agreement_get_section_default")]
    #[doc(alias = "get_section_default")]
    fn section_default(&self) -> Option<AgreementSection> {
        unsafe {
            from_glib_none(ffi::as_agreement_get_section_default(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "as_agreement_get_sections")]
    #[doc(alias = "get_sections")]
    fn sections(&self) -> Vec<AgreementSection> {
        unsafe {
            FromGlibPtrContainer::from_glib_none(ffi::as_agreement_get_sections(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "as_agreement_get_version_id")]
    #[doc(alias = "get_version_id")]
    fn version_id(&self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::as_agreement_get_version_id(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "as_agreement_set_kind")]
    fn set_kind(&self, kind: AgreementKind) {
        unsafe {
            ffi::as_agreement_set_kind(self.as_ref().to_glib_none().0, kind.into_glib());
        }
    }

    #[doc(alias = "as_agreement_set_version_id")]
    fn set_version_id(&self, version_id: &str) {
        unsafe {
            ffi::as_agreement_set_version_id(
                self.as_ref().to_glib_none().0,
                version_id.to_glib_none().0,
            );
        }
    }
}

impl<O: IsA<Agreement>> AgreementExt for O {}
