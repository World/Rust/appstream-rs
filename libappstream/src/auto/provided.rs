// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, ProvidedKind};
use glib::{prelude::*, translate::*};

glib::wrapper! {
    #[doc(alias = "AsProvided")]
    pub struct Provided(Object<ffi::AsProvided, ffi::AsProvidedClass>);

    match fn {
        type_ => || ffi::as_provided_get_type(),
    }
}

impl Provided {
    pub const NONE: Option<&'static Provided> = None;

    #[doc(alias = "as_provided_new")]
    pub fn new() -> Provided {
        assert_initialized_main_thread!();
        unsafe { from_glib_full(ffi::as_provided_new()) }
    }
}

impl Default for Provided {
    fn default() -> Self {
        Self::new()
    }
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Provided>> Sealed for T {}
}

pub trait ProvidedExt: IsA<Provided> + sealed::Sealed + 'static {
    #[doc(alias = "as_provided_add_item")]
    fn add_item(&self, item: &str) {
        unsafe {
            ffi::as_provided_add_item(self.as_ref().to_glib_none().0, item.to_glib_none().0);
        }
    }

    #[doc(alias = "as_provided_get_items")]
    #[doc(alias = "get_items")]
    fn items(&self) -> Vec<glib::GString> {
        unsafe {
            FromGlibPtrContainer::from_glib_none(ffi::as_provided_get_items(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "as_provided_get_kind")]
    #[doc(alias = "get_kind")]
    fn kind(&self) -> ProvidedKind {
        unsafe { from_glib(ffi::as_provided_get_kind(self.as_ref().to_glib_none().0)) }
    }

    #[doc(alias = "as_provided_has_item")]
    fn has_item(&self, item: &str) -> bool {
        unsafe {
            from_glib(ffi::as_provided_has_item(
                self.as_ref().to_glib_none().0,
                item.to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "as_provided_set_kind")]
    fn set_kind(&self, kind: ProvidedKind) {
        unsafe {
            ffi::as_provided_set_kind(self.as_ref().to_glib_none().0, kind.into_glib());
        }
    }
}

impl<O: IsA<Provided>> ProvidedExt for O {}
