// Generated by gir (https://github.com/gtk-rs/gir @ 5327f5be4615)
// from 
// from gir-files (https://github.com/gtk-rs/gir-files.git @ 62da9eb7c4bd)
// DO NOT EDIT

#include "manual.h"
#include <stdio.h>

#define PRINT_CONSTANT(CONSTANT_NAME) \
    printf("%s;", #CONSTANT_NAME); \
    printf(_Generic((CONSTANT_NAME), \
                    char *: "%s", \
                    const char *: "%s", \
                    char: "%c", \
                    signed char: "%hhd", \
                    unsigned char: "%hhu", \
                    short int: "%hd", \
                    unsigned short int: "%hu", \
                    int: "%d", \
                    unsigned int: "%u", \
                    long: "%ld", \
                    unsigned long: "%lu", \
                    long long: "%lld", \
                    unsigned long long: "%llu", \
                    float: "%f", \
                    double: "%f", \
                    long double: "%ld"), \
           CONSTANT_NAME); \
    printf("\n");

int main() {
    PRINT_CONSTANT((gint) AS_AGREEMENT_KIND_EULA);
    PRINT_CONSTANT((gint) AS_AGREEMENT_KIND_GENERIC);
    PRINT_CONSTANT((gint) AS_AGREEMENT_KIND_PRIVACY);
    PRINT_CONSTANT((gint) AS_AGREEMENT_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_ARTIFACT_KIND_BINARY);
    PRINT_CONSTANT((gint) AS_ARTIFACT_KIND_SOURCE);
    PRINT_CONSTANT((gint) AS_ARTIFACT_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_APPIMAGE);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_CABINET);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_FLATPAK);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_LIMBA);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_LINGLONG);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_PACKAGE);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_SNAP);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_TARBALL);
    PRINT_CONSTANT((gint) AS_BUNDLE_KIND_UNKNOWN);
    PRINT_CONSTANT((guint) AS_CACHE_FLAG_NONE);
    PRINT_CONSTANT((guint) AS_CACHE_FLAG_NO_CLEAR);
    PRINT_CONSTANT((guint) AS_CACHE_FLAG_REFRESH_SYSTEM);
    PRINT_CONSTANT((guint) AS_CACHE_FLAG_USE_SYSTEM);
    PRINT_CONSTANT((guint) AS_CACHE_FLAG_USE_USER);
    PRINT_CONSTANT((gint) AS_CHASSIS_KIND_DESKTOP);
    PRINT_CONSTANT((gint) AS_CHASSIS_KIND_HANDSET);
    PRINT_CONSTANT((gint) AS_CHASSIS_KIND_LAPTOP);
    PRINT_CONSTANT((gint) AS_CHASSIS_KIND_SERVER);
    PRINT_CONSTANT((gint) AS_CHASSIS_KIND_TABLET);
    PRINT_CONSTANT((gint) AS_CHASSIS_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_CHECKSUM_KIND_BLAKE2B);
    PRINT_CONSTANT((gint) AS_CHECKSUM_KIND_BLAKE3);
    PRINT_CONSTANT((gint) AS_CHECKSUM_KIND_NONE);
    PRINT_CONSTANT((gint) AS_CHECKSUM_KIND_SHA1);
    PRINT_CONSTANT((gint) AS_CHECKSUM_KIND_SHA256);
    PRINT_CONSTANT((gint) AS_CHECKSUM_KIND_SHA512);
    PRINT_CONSTANT((gint) AS_CHECK_RESULT_ERROR);
    PRINT_CONSTANT((gint) AS_CHECK_RESULT_FALSE);
    PRINT_CONSTANT((gint) AS_CHECK_RESULT_TRUE);
    PRINT_CONSTANT((gint) AS_CHECK_RESULT_UNKNOWN);
    PRINT_CONSTANT((gint) AS_COLOR_KIND_PRIMARY);
    PRINT_CONSTANT((gint) AS_COLOR_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_COLOR_SCHEME_KIND_DARK);
    PRINT_CONSTANT((gint) AS_COLOR_SCHEME_KIND_LIGHT);
    PRINT_CONSTANT((gint) AS_COLOR_SCHEME_KIND_UNKNOWN);
    PRINT_CONSTANT((guint) AS_COMPONENT_BOX_FLAG_NONE);
    PRINT_CONSTANT((guint) AS_COMPONENT_BOX_FLAG_NO_CHECKS);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_ADDON);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_CODEC);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_CONSOLE_APP);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_DESKTOP_APP);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_DRIVER);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_FIRMWARE);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_FONT);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_GENERIC);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_ICON_THEME);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_INPUT_METHOD);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_LOCALIZATION);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_OPERATING_SYSTEM);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_REPOSITORY);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_RUNTIME);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_SERVICE);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_COMPONENT_KIND_WEB_APP);
    PRINT_CONSTANT((gint) AS_COMPONENT_SCOPE_SYSTEM);
    PRINT_CONSTANT((gint) AS_COMPONENT_SCOPE_UNKNOWN);
    PRINT_CONSTANT((gint) AS_COMPONENT_SCOPE_USER);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_ACB);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_CERO);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_DJCTQ);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_ESRA);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_ESRB);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_GRAC);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_GSRR);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_IARC);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_INCAA);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_KAVI);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_MDA);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_OFLCNZ);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_PEGI);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_RUSSIA);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_UNKNOWN);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_SYSTEM_USK);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_VALUE_INTENSE);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_VALUE_MILD);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_VALUE_MODERATE);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_VALUE_NONE);
    PRINT_CONSTANT((gint) AS_CONTENT_RATING_VALUE_UNKNOWN);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_CONSOLE);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_GAMEPAD);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_KEYBOARD);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_POINTING);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_TABLET);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_TOUCH);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_TV_REMOTE);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_VISION);
    PRINT_CONSTANT((gint) AS_CONTROL_KIND_VOICE);
    PRINT_CONSTANT((guint) AS_DATA_ID_MATCH_FLAG_BRANCH);
    PRINT_CONSTANT((guint) AS_DATA_ID_MATCH_FLAG_BUNDLE_KIND);
    PRINT_CONSTANT((guint) AS_DATA_ID_MATCH_FLAG_ID);
    PRINT_CONSTANT((guint) AS_DATA_ID_MATCH_FLAG_NONE);
    PRINT_CONSTANT((guint) AS_DATA_ID_MATCH_FLAG_ORIGIN);
    PRINT_CONSTANT((guint) AS_DATA_ID_MATCH_FLAG_SCOPE);
    PRINT_CONSTANT((gint) AS_DISPLAY_SIDE_KIND_LONGEST);
    PRINT_CONSTANT((gint) AS_DISPLAY_SIDE_KIND_SHORTEST);
    PRINT_CONSTANT((gint) AS_DISPLAY_SIDE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_FORMAT_KIND_DESKTOP_ENTRY);
    PRINT_CONSTANT((gint) AS_FORMAT_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_FORMAT_KIND_XML);
    PRINT_CONSTANT((gint) AS_FORMAT_KIND_YAML);
    PRINT_CONSTANT((gint) AS_FORMAT_STYLE_CATALOG);
    PRINT_CONSTANT((gint) AS_FORMAT_STYLE_METAINFO);
    PRINT_CONSTANT((gint) AS_FORMAT_STYLE_UNKNOWN);
    PRINT_CONSTANT((gint) AS_FORMAT_VERSION_UNKNOWN);
    PRINT_CONSTANT((gint) AS_FORMAT_VERSION_V1_0);
    PRINT_CONSTANT((gint) AS_ICON_KIND_CACHED);
    PRINT_CONSTANT((gint) AS_ICON_KIND_LOCAL);
    PRINT_CONSTANT((gint) AS_ICON_KIND_REMOTE);
    PRINT_CONSTANT((gint) AS_ICON_KIND_STOCK);
    PRINT_CONSTANT((gint) AS_ICON_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_IMAGE_KIND_SOURCE);
    PRINT_CONSTANT((gint) AS_IMAGE_KIND_THUMBNAIL);
    PRINT_CONSTANT((gint) AS_IMAGE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_INTERNET_KIND_ALWAYS);
    PRINT_CONSTANT((gint) AS_INTERNET_KIND_FIRST_RUN);
    PRINT_CONSTANT((gint) AS_INTERNET_KIND_OFFLINE_ONLY);
    PRINT_CONSTANT((gint) AS_INTERNET_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_ISSUE_KIND_CVE);
    PRINT_CONSTANT((gint) AS_ISSUE_KIND_GENERIC);
    PRINT_CONSTANT((gint) AS_ISSUE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_ISSUE_SEVERITY_ERROR);
    PRINT_CONSTANT((gint) AS_ISSUE_SEVERITY_INFO);
    PRINT_CONSTANT((gint) AS_ISSUE_SEVERITY_PEDANTIC);
    PRINT_CONSTANT((gint) AS_ISSUE_SEVERITY_UNKNOWN);
    PRINT_CONSTANT((gint) AS_ISSUE_SEVERITY_WARNING);
    PRINT_CONSTANT((gint) AS_LAUNCHABLE_KIND_COCKPIT_MANIFEST);
    PRINT_CONSTANT((gint) AS_LAUNCHABLE_KIND_DESKTOP_ID);
    PRINT_CONSTANT((gint) AS_LAUNCHABLE_KIND_SERVICE);
    PRINT_CONSTANT((gint) AS_LAUNCHABLE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_LAUNCHABLE_KIND_URL);
    PRINT_CONSTANT((gint) AS_MARKUP_KIND_MARKDOWN);
    PRINT_CONSTANT((gint) AS_MARKUP_KIND_TEXT);
    PRINT_CONSTANT((gint) AS_MARKUP_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_MARKUP_KIND_XML);
    PRINT_CONSTANT((gint) AS_MERGE_KIND_APPEND);
    PRINT_CONSTANT((gint) AS_MERGE_KIND_NONE);
    PRINT_CONSTANT((gint) AS_MERGE_KIND_REMOVE_COMPONENT);
    PRINT_CONSTANT((gint) AS_MERGE_KIND_REPLACE);
    PRINT_CONSTANT((gint) AS_METADATA_ERROR_FAILED);
    PRINT_CONSTANT((gint) AS_METADATA_ERROR_FORMAT_UNEXPECTED);
    PRINT_CONSTANT((gint) AS_METADATA_ERROR_NO_COMPONENT);
    PRINT_CONSTANT((gint) AS_METADATA_ERROR_PARSE);
    PRINT_CONSTANT((gint) AS_METADATA_ERROR_VALUE_MISSING);
    PRINT_CONSTANT((gint) AS_METADATA_LOCATION_CACHE);
    PRINT_CONSTANT((gint) AS_METADATA_LOCATION_SHARED);
    PRINT_CONSTANT((gint) AS_METADATA_LOCATION_STATE);
    PRINT_CONSTANT((gint) AS_METADATA_LOCATION_UNKNOWN);
    PRINT_CONSTANT((gint) AS_METADATA_LOCATION_USER);
    PRINT_CONSTANT((guint) AS_PARSE_FLAG_IGNORE_MEDIABASEURL);
    PRINT_CONSTANT((guint) AS_PARSE_FLAG_NONE);
    PRINT_CONSTANT((gint) AS_POOL_ERROR_CACHE_DAMAGED);
    PRINT_CONSTANT((gint) AS_POOL_ERROR_CACHE_WRITE_FAILED);
    PRINT_CONSTANT((gint) AS_POOL_ERROR_COLLISION);
    PRINT_CONSTANT((gint) AS_POOL_ERROR_FAILED);
    PRINT_CONSTANT((gint) AS_POOL_ERROR_INCOMPLETE);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_IGNORE_CACHE_AGE);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_LOAD_FLATPAK);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_LOAD_OS_CATALOG);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_LOAD_OS_DESKTOP_FILES);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_LOAD_OS_METAINFO);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_MONITOR);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_NONE);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_PREFER_OS_METAINFO);
    PRINT_CONSTANT((guint) AS_POOL_FLAG_RESOLVE_ADDONS);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_BINARY);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_DBUS_SYSTEM);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_DBUS_USER);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_FIRMWARE_FLASHED);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_FIRMWARE_RUNTIME);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_FONT);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_ID);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_LIBRARY);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_MEDIATYPE);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_MODALIAS);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_PYTHON);
    PRINT_CONSTANT((gint) AS_PROVIDED_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_REFERENCE_KIND_CITATION_CFF);
    PRINT_CONSTANT((gint) AS_REFERENCE_KIND_DOI);
    PRINT_CONSTANT((gint) AS_REFERENCE_KIND_REGISTRY);
    PRINT_CONSTANT((gint) AS_REFERENCE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_EQ);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_GE);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_GT);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_LE);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_LT);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_NE);
    PRINT_CONSTANT((gint) AS_RELATION_COMPARE_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELATION_ERROR_BAD_VALUE);
    PRINT_CONSTANT((gint) AS_RELATION_ERROR_FAILED);
    PRINT_CONSTANT((gint) AS_RELATION_ERROR_NOT_IMPLEMENTED);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_CONTROL);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_DISPLAY_LENGTH);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_FIRMWARE);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_HARDWARE);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_ID);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_INTERNET);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_KERNEL);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_MEMORY);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_MODALIAS);
    PRINT_CONSTANT((gint) AS_RELATION_ITEM_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELATION_KIND_RECOMMENDS);
    PRINT_CONSTANT((gint) AS_RELATION_KIND_REQUIRES);
    PRINT_CONSTANT((gint) AS_RELATION_KIND_SUPPORTS);
    PRINT_CONSTANT((gint) AS_RELATION_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELATION_STATUS_ERROR);
    PRINT_CONSTANT((gint) AS_RELATION_STATUS_NOT_SATISFIED);
    PRINT_CONSTANT((gint) AS_RELATION_STATUS_SATISFIED);
    PRINT_CONSTANT((gint) AS_RELATION_STATUS_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELEASE_KIND_DEVELOPMENT);
    PRINT_CONSTANT((gint) AS_RELEASE_KIND_SNAPSHOT);
    PRINT_CONSTANT((gint) AS_RELEASE_KIND_STABLE);
    PRINT_CONSTANT((gint) AS_RELEASE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELEASE_LIST_KIND_EMBEDDED);
    PRINT_CONSTANT((gint) AS_RELEASE_LIST_KIND_EXTERNAL);
    PRINT_CONSTANT((gint) AS_RELEASE_LIST_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_RELEASE_URL_KIND_DETAILS);
    PRINT_CONSTANT((gint) AS_RELEASE_URL_KIND_UNKNOWN);
    PRINT_CONSTANT((guint) AS_REVIEW_FLAG_NONE);
    PRINT_CONSTANT((guint) AS_REVIEW_FLAG_SELF);
    PRINT_CONSTANT((guint) AS_REVIEW_FLAG_VOTED);
    PRINT_CONSTANT((gint) AS_SCREENSHOT_KIND_DEFAULT);
    PRINT_CONSTANT((gint) AS_SCREENSHOT_KIND_EXTRA);
    PRINT_CONSTANT((gint) AS_SCREENSHOT_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_SCREENSHOT_MEDIA_KIND_IMAGE);
    PRINT_CONSTANT((gint) AS_SCREENSHOT_MEDIA_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_SCREENSHOT_MEDIA_KIND_VIDEO);
    PRINT_CONSTANT((gint) AS_SIZE_KIND_DOWNLOAD);
    PRINT_CONSTANT((gint) AS_SIZE_KIND_INSTALLED);
    PRINT_CONSTANT((gint) AS_SIZE_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_SUGGESTED_KIND_HEURISTIC);
    PRINT_CONSTANT((gint) AS_SUGGESTED_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_SUGGESTED_KIND_UPSTREAM);
    PRINT_CONSTANT((gint) AS_SYSTEM_INFO_ERROR_FAILED);
    PRINT_CONSTANT((gint) AS_SYSTEM_INFO_ERROR_NOT_FOUND);
    PRINT_CONSTANT((gint) AS_TRANSLATION_KIND_GETTEXT);
    PRINT_CONSTANT((gint) AS_TRANSLATION_KIND_QT);
    PRINT_CONSTANT((gint) AS_TRANSLATION_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_URGENCY_KIND_CRITICAL);
    PRINT_CONSTANT((gint) AS_URGENCY_KIND_HIGH);
    PRINT_CONSTANT((gint) AS_URGENCY_KIND_LOW);
    PRINT_CONSTANT((gint) AS_URGENCY_KIND_MEDIUM);
    PRINT_CONSTANT((gint) AS_URGENCY_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_URL_KIND_BUGTRACKER);
    PRINT_CONSTANT((gint) AS_URL_KIND_CONTACT);
    PRINT_CONSTANT((gint) AS_URL_KIND_CONTRIBUTE);
    PRINT_CONSTANT((gint) AS_URL_KIND_DONATION);
    PRINT_CONSTANT((gint) AS_URL_KIND_FAQ);
    PRINT_CONSTANT((gint) AS_URL_KIND_HELP);
    PRINT_CONSTANT((gint) AS_URL_KIND_HOMEPAGE);
    PRINT_CONSTANT((gint) AS_URL_KIND_TRANSLATE);
    PRINT_CONSTANT((gint) AS_URL_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_URL_KIND_VCS_BROWSER);
    PRINT_CONSTANT((gint) AS_UTILS_ERROR_FAILED);
    PRINT_CONSTANT((gint) AS_VALIDATOR_ERROR_FAILED);
    PRINT_CONSTANT((gint) AS_VALIDATOR_ERROR_INVALID_FILENAME);
    PRINT_CONSTANT((gint) AS_VALIDATOR_ERROR_INVALID_OVERRIDE);
    PRINT_CONSTANT((guint) AS_VALUE_FLAG_DUPLICATE_CHECK);
    PRINT_CONSTANT((guint) AS_VALUE_FLAG_NONE);
    PRINT_CONSTANT((guint) AS_VALUE_FLAG_NO_TRANSLATION_FALLBACK);
    PRINT_CONSTANT((guint) AS_VERCMP_FLAG_IGNORE_EPOCH);
    PRINT_CONSTANT((guint) AS_VERCMP_FLAG_NONE);
    PRINT_CONSTANT((gint) AS_VIDEO_CODEC_KIND_AV1);
    PRINT_CONSTANT((gint) AS_VIDEO_CODEC_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_VIDEO_CODEC_KIND_VP9);
    PRINT_CONSTANT((gint) AS_VIDEO_CONTAINER_KIND_MKV);
    PRINT_CONSTANT((gint) AS_VIDEO_CONTAINER_KIND_UNKNOWN);
    PRINT_CONSTANT((gint) AS_VIDEO_CONTAINER_KIND_WEBM);
    return 0;
}
