use libc::c_ushort;

pub type AsSearchTokenMatch = c_ushort;
pub const AS_SEARCH_TOKEN_MATCH_NONE: AsSearchTokenMatch = 0;
pub const AS_SEARCH_TOKEN_MATCH_MEDIATYPE: AsSearchTokenMatch = 1;
pub const AS_SEARCH_TOKEN_MATCH_PKGNAME: AsSearchTokenMatch = 2;
pub const AS_SEARCH_TOKEN_MATCH_ORIGIN: AsSearchTokenMatch = 4;
pub const AS_SEARCH_TOKEN_MATCH_DESCRIPTION: AsSearchTokenMatch = 8;
pub const AS_SEARCH_TOKEN_MATCH_SUMMARY: AsSearchTokenMatch = 16;
pub const AS_SEARCH_TOKEN_MATCH_KEYWORD: AsSearchTokenMatch = 32;
pub const AS_SEARCH_TOKEN_MATCH_NAME: AsSearchTokenMatch = 64;
pub const AS_SEARCH_TOKEN_MATCH_ID: AsSearchTokenMatch = 128;
